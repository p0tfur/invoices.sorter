#Introduction
Processing of invoices is playing a vital role in large companies. Usually, invoices are classified manually by humans, who have to pick them up from the pile of all documents like reminders, statements, orders, delivery notes, receipts and so on. This manual process is time-consuming and at risk of errors because of various templates and large volumens. For these reasons, automatic analysis and sorting of incoming documents is essential.

# Invoices Sorter Robot
is a MIT licensed complete solution for sorting incoming e-mails and their attachments (invoices, statements, reminders etc.) in Accounts Payable or Accounts Receivable department.

Solution handles process of sorting from A to Z, so from incoming e-mail to your mailbox to invoice saved on your drive ready to be uploaded to SAP or other ERP/Finance system. It also takes care to not let you omit any important message i.e.: payment reminder. 

The main goal of Robot is to ease the workload of the Accounting Team and to save time and money for the company. Automating invoices sorting process gives humans opportunity to put their attention on other more important tasks.

Whole project consists of ready to use simple workflow, which is ready to use, as is already calibrated to sort english invoices, or can be used as a base to develop your own solution. Additionaly contains three powerful sets of custom activities, which has been developed for Invoices Sorter, but are also highly reusable and can be used in other processes also.

Contains also **Invoices Sorter Assistant** workflow, the main goal of assistant is to pick up attachments from e-mails sorted by human and save them to correct entities folders on drive. More details in manual.

## Activities set
#### Invoices Sorter Activities
This package takes care about most important steps of the sorting process. But can be also used to build your own sorting robot for any type of documents.

*  Analyze PDF,
*  Count Attachments, 
*  Save Attachment By Index, 
*  Add String To List, 
*  Create Drive Directories.

[For more details check repository.](https://bitbucket.org/p0tfur/invoices.sorter.activities/)

#### Exchange Mail Activities
Package of several activities to manage mailbox using Microsoft Exchange Webservices (EWS). At this moment this is only fully working set of activites based on EWS for UiPath:

*  handle up to 500 folders (500 is max for Exchange server),
*  read as many messages from server as you want (there is no limit to 1000 messages!) also from Sharedmailbox,
*  save all file attachments, filtering by extension if needed, and removing forbidden chars from file names, so file is saved instead of throwing exception (yes, a lot of invoicing systems use forbidden characters for invoices files names),
*  [and a lot more! Check repository for details.](https://bitbucket.org/p0tfur/exchange.mail.activities/)

Contains activites:

*  Convert_String_To_SecureString
*  Create Exchange Connection,
*  Create Exchange Directories,
*  Get Exchange Messages,
*  Move Message,
*  Save Attachments,
*  Retrieve Attachment,
*  Send e-mail.

#### Text Analysis Activities:
Text Analysis (or Text Mining) is the automated process of obtaining information from text, the goal is to create structured data out of free text content, which can be easier interpreted by computer. It can be used for categorizing press articles, user reviews, incoming e-mails, tickets, monitoring comments or social media and many many more.

First release contains several basic, but powerful methods and techniques:

*  Detect Language, 
*  Find Collocations,
*  Prepare String To Analyze,
*  Remove Stopwords,
*  Word Frequency Analyse,
*  Word Position Analyse.

[For more details check dedicated repository.](https://bitbucket.org/p0tfur/text.analysis.activities/)